
Pod::Spec.new do |s|
  s.name             = 'Visualytics'
  s.version          = '0.2.3'
  s.summary          = 'Visualytics is an UI analytics and visualization tool.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://visualytics.xyz'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'aobondar' => 'artem@desq.me' }
  # s.social_media_url = 'https://twitter.com/aobondar'

  s.ios.deployment_target = '9.0'
  s.source = { :http => 'https://s3.amazonaws.com/desqlabs-pods/visualytics/visualytics7.zip' }
  s.ios.vendored_frameworks = 'Visualytics.framework'

end
