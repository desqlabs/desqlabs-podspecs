#
# Be sure to run `pod lib lint Briefly.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Briefly'
  s.version          = '0.2.2'
  s.summary          = 'Briefly is an analytics visualization tool.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/partyappdev/briefly-ios-sdk'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'aobondar' => 'artem@desq.me' }
  s.source           = { :git => 'https://bitbucket.org/partyappdev/briefly-ios-sdk.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/aobondar'

  s.ios.deployment_target = '9.0'
  s.dependency 'JWT'
  s.dependency 'Base64'
  s.source_files = 'Briefly/Classes/**/*'

  s.public_header_files = 'Briefly/Classes/Briefly.h', 'Briefly/Classes/UI/Categories/UIView+Tracking.h', 'Briefly/Classes/UI/Categories/UIViewController+Tracking.h', 'Briefly/Classes/UI/Views/BFStatisticsLabel.h', 'Briefly/Classes/UI/Views/BFPurchasesStatisticLabel.h', 'Briefly/Classes/BrieflyFacade.h', 'Briefly/Classes/BFSortableDataSource.h', 'Briefly/Classes/BFSorter.h'
  
  s.resource_bundles = {
    'Briefly' => ['Briefly/Assets/*.{lproj,storyboard,xcassets,png,strings,pem,xib}']
  }

end
