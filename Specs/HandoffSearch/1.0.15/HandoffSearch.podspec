
Pod::Spec.new do |s|
  s.name             = 'HandoffSearch'
  s.version          = '1.0.15'
  s.summary          = 'HandoffSearch is an voice recognition service'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'aobondar' => 'artem@desq.me' }
  # s.social_media_url = 'https://twitter.com/aobondar'

  s.ios.deployment_target = '9.0'
  s.source = { :http => 'https://s3.amazonaws.com/desqlabs-pods/HandoffSearch/HandoffSearch24.zip' }
  s.ios.vendored_frameworks = 'HandoffSearch.framework'

end
