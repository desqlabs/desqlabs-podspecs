import click
import os
import re
from subprocess import call

class VersionTuple:
	major = None
	minor = None
	build = None

	def __init__(self, raw_string):
		splitted = raw_string.split('.')
		self.major = int(splitted[0])
		self.minor = int(splitted[1])
		self.build = int(splitted[2])

	def ver_string(self):
		return u'{}.{}.{}'.format(self.major, self.minor, self.build)

	def incremented(self, pos):
		new_obj = VersionTuple(self.ver_string())
		if pos == 0:
			new_obj.major += 1
		elif pos == 1:
			new_obj.minor += 1
		elif pos == 2:
			new_obj.build += 1
		return new_obj

	def is_newer_than(self, other):
		if self.major > other.major:
			return True
		elif self.minor > other.minor:
			return True
		elif self.build > other.build:
			return True
		return False

@click.command()
@click.option('--name', prompt='Package name', help='PName of packege to update')
@click.option('--increment-version-pos', default=2, help='0 - major version, 1 - minor version, 2 - build num (default)')
@click.option('--source-url', default=None, help='If defined - will change the source url')
def new_version(name, increment_version_pos, source_url):
	#
	# Find the last version, and create a bare copy
	#
	d = u'./Specs/' + name
	last_version = VersionTuple('0.0.0')
	for o in os.listdir(d):
		if os.path.isdir(os.path.join(d,o)):
			current_version = VersionTuple(o)
			if current_version.is_newer_than(last_version):
				last_version = current_version

	new_version = last_version.incremented(increment_version_pos)

	# configure pathes for old podspec and new one
	prev_version_dir = os.path.join(d, last_version.ver_string())
	new_version_dir = os.path.join(d, new_version.ver_string())
	prev_version_spec = os.path.join(prev_version_dir, u'{}.podspec'.format(name))
	new_version_spec = os.path.join(new_version_dir, u'{}.podspec'.format(name))

	old_specs_lines = []
	new_specs_lines = []
	with open(prev_version_spec, 'r') as fp:
		old_specs_lines = fp.readlines()
	print('Read {}. len={}'.format(prev_version_spec, len(old_specs_lines)))

	# start modifications
	for old_line in old_specs_lines:
		if u'.version' in old_line:
			new_line = re.sub(r'\'[^\']*\'', u'\'' + new_version.ver_string() + u'\'', old_line)
			new_specs_lines.append(new_line)
		elif u'.source' in old_line and source_url is not None:
			new_line = re.sub(r'\'[^\']*\'', u'\'' + source_url + u'\'', old_line)
			new_specs_lines.append(new_line)
		else:
			new_specs_lines.append(old_line)
		print('{}->{}'.format(old_line, new_specs_lines[-1]))

	# write new podspec to file
	if not os.path.exists(new_version_dir):
		os.makedirs(new_version_dir)
	with open(new_version_spec, 'w') as fp:
		for line in new_specs_lines:
			fp.write(line)

	# commit this one to git
	call(["git", "add", "."])
	call(["git", "commit", "-m", "New version ({}) for {}".format(new_version.ver_string(), name)])

	# push to the repo
	call(["git", "push"])

	# update local podspecs (pod repo update bitbucket-desqlabs-desqlabs-podspecs)
	call(["pod", "repo", "update", "bitbucket-desqlabs-desqlabs-podspecs"])

if __name__ == '__main__':
	new_version()
